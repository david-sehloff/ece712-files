close all
clear
%dc bus characteristics
step = 1e-6;
t_rise = 10e-3;
t_fall = 20e-3;
amp = 100;
offset = 350;
start = 0;
stop = 90e-3;

[t,Vdc] = triang_wave(amp,t_rise,t_fall,offset,start,stop,step);

%ac output characteristics
Vacpk = 120*sqrt(2);
Iacpk = 10*sqrt(2);
w_o = 60*2*pi;

Vac = Vacpk*cos(w_o*t);
Iac = Iacpk*cos(w_o*t);

figure
set(gcf, 'PaperPosition', [0 0 12.0 8.0]);
subplot(3,1,1)
plot(t*1000,Vdc)
xlabel('time (ms)')
ylabel('dc bus voltage (V)')

subplot(3,1,2)
plot(t*1000,Vac)
xlabel('time (ms)')
ylabel('ac output voltage (V)')

subplot(3,1,3)
plot(t*1000,Iac)
xlabel('time (ms)')
ylabel('fund. output current (A)')

print('input_output','-depsc');

%Modulation
Vout_ref = Vac;
m = Vout_ref./Vdc;
m11 = (1+m)/2;
m21 = (1-m)/2;
m12 = (1-m)/2;
m22 = (1+m)/2;

Vout_av = Vdc.*m11-Vdc.*m12;

figure
set(gcf, 'PaperPosition', [0 0 12.0 8.0]);
subplot(4,1,1)
plot(t*1000,m11)
xlabel('time (ms)')
ylabel('m_{11}')

subplot(4,1,2)
plot(t*1000,m21)
xlabel('time (ms)')
ylabel('m_{21}')

subplot(4,1,3)
plot(t*1000,m12)
xlabel('time (ms)')
ylabel('m_{12}')

subplot(4,1,4)
plot(t*1000,m22)
xlabel('time (ms)')
ylabel('m_{22}')

print('modulation','-depsc');

figure
set(gcf, 'PaperPosition', [0 0 12.0 3.0]);
plot(t*1000,Vout_av)
xlabel('time (ms)')
ylabel('average output voltage (V)')
print('output_av','-depsc');

%carrier
step = 1e-6;
t_rise = 5e-5;
t_fall = 5e-5;
amp = 1;
offset = 0.5;

[~,carrier] = triang_wave(amp,t_rise,t_fall,offset,start,stop,step);
h11 = m11 > carrier;
h21 = m11 <= carrier;
h12 = m12 > carrier;
h22 = m12 <= carrier;

figure
set(gcf, 'PaperPosition', [0 0 12.0 8.0]);
subplot(5,1,1)
plot(t*1000,carrier,t*1000,m11,t*1000,m12)
legend('carrier','m_{11}','m_{12}')
axis([0 .7 -inf inf])
xlabel('time (ms)')
ylabel('carrier waveform')
subplot(5,1,2)
plot(t*1000,h11)
axis([0 .7 -inf 1.2])
xlabel('time (ms)')
ylabel('h_{11}')
subplot(5,1,3)
plot(t*1000,h21)
axis([0 .7 -inf 1.2])
xlabel('time (ms)')
ylabel('h_{21}')
subplot(5,1,4)
plot(t*1000,h12)
axis([0 .7 -inf 1.2])
xlabel('time (ms)')
ylabel('h_{12}')
subplot(5,1,5)
plot(t*1000,h22)
axis([0 .7 -inf 1.2])
xlabel('time (ms)')
ylabel('h_{22}')
print('switching','-depsc');

%output voltage
windowSize = 50; 
b = (1/windowSize)*ones(1,windowSize);
a = 1;

Vout = Vdc.*h11-Vdc.*h12;
Vout_filt = filter(b,a,Vout);

figure
set(gcf, 'PaperPosition', [0 0 12.0 3.0]);
plot(t*1000,Vout)
xlabel('time (ms)')
ylabel('output voltage (V)')
axis([0 60 -inf inf])
print('output','-depsc');
axis([0 0.7 0 inf])
print('output_zoom','-depsc');
axis([4.15 12.5 -inf 0])
print('output_zoom2','-depsc');

figure
set(gcf, 'PaperPosition', [0 0 12.0 3.0]);
plot(t*1000,Vout_filt)
xlabel('time (ms)')
ylabel('filtered output voltage (V)')
axis([0 60 -inf inf])
print('output_filt','-depsc');

%output current
Idc = Iac.*(h11 .* h22) - Iac.*(h21 .* h12);
figure
set(gcf, 'PaperPosition', [0 0 12.0 3.0]);
plot(t*1000,Idc)
xlabel('time (ms)')
ylabel('dc bus current (A)')
axis([0 60 -inf inf])
print('buscurrent','-depsc');
axis([0 0.7 0 inf])
print('buscurrent_zoom','-depsc');

figure
set(gcf, 'PaperPosition', [0 0 12.0 3.0]);
plot(t*1000,((h11 .* h22)-(h21 .* h12)))
xlabel('time (ms)')
ylabel('h_{11}h_{22}-h_{21}h_{12}')
axis([0 60 -inf inf])
print('switch_mult','-depsc');
