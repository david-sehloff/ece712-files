v_in_rms = 1/sqrt(2);
v_in_pk = v_in_rms * sqrt(2);
theta = linspace(-2*pi,2*pi,1e3);
va = v_in_pk*cos(theta);
vb = v_in_pk*cos(theta-2*pi/3);
vc = v_in_pk*cos(theta+2*pi/3);
vab = va-vb;
vbc = vb-vc;
vca = vc-va;
close all
figure
plot(theta/pi, vab,theta/pi, vbc,theta/pi, vca)
xlabel('x \pi')
figure
plot(theta/pi, va,theta/pi, vb,theta/pi, vc)
xlabel('x \pi')

va = v_in_pk*cos(theta);
ia = cos(5*theta);
figure
plot(theta, va.*ia)