
t=0:0.000001:0.04;
w0 = 400*2*pi;
wi = 60*2*pi;
alpha_o0 = 0;
beta_i0 = 0;

alpha_i0 = 0;
beta_o0 = 0;

alpha_o = w0*t + alpha_o0;
beta_i = wi*t + beta_i0;

alpha_i = wi*t + alpha_i0;
beta_o = w0*t + beta_o0;

ma = cos(beta_i);
mb = cos(beta_i-2*pi/3);
mc = cos(beta_i+2*pi/3);

mu = cos(alpha_o);
mv = cos(alpha_o-2*pi/3);
mw = cos(alpha_o+2*pi/3);

hap = ma>mb & ma>mc;
hbp = mb>ma & mb>mc;
hcp = mc>ma & mc>mb;
han = ma<mb & ma<mc;
hbn = mb<ma & mb<mc;
hcn = mc<ma & mc<mb;

hup = mu > 0;
hvp = mv > 0;
hwp = mw > 0;
hun = 1-hup;
hvn = 1-hvp;
hwn = 1-hwp;

%Hvsb = [{hup} {hun};{hvp} {hvn};{hwp} {hwn}];
%Hcsb = [{hap} {hbp} {hcp};{han} {hbn} {hcn}];
hau = hup.*hap+hun.*han;
hbu = hup.*hbp+hun.*hbn;
hcu = hup.*hcp+hun.*hcn;
hav = hvp.*hap+hvn.*han;
hbv = hvp.*hbp+hvn.*hbn;
hcv = hvp.*hcp+hvn.*hcn;
haw = hwp.*hap+hwn.*han;
hbw = hwp.*hbp+hwn.*hbn;
hcw = hwp.*hcp+hwn.*hcn;

Hcmc = [{hau} {hbu} {hcu};
        {hav} {hbv} {hcv};
        {haw} {hbw} {hcw}];

vin = 230*sqrt(2)/sqrt(3);
vb = 230*sqrt(2);
vi = vin;
va=vi*cos(alpha_i);
vb=vi*cos(alpha_i-2*pi/3);
vc=vi*cos(alpha_i+2*pi/3);

vu=Hcmc{1,1}.*va+Hcmc{1,2}.*vb+Hcmc{1,3}.*vc;
vv=Hcmc{2,1}.*va+Hcmc{2,2}.*vb+Hcmc{2,3}.*vc;
vw=Hcmc{3,1}.*va+Hcmc{3,2}.*vb+Hcmc{3,3}.*vc;

io=10*sqrt(2);
iu=io*cos(beta_o);
iv=io*cos(beta_o-2*pi/3);
iw=io*cos(beta_o+2*pi/3);



iau=hau.*iu;
ibu=hbu.*iu;
icu=hcu.*iu;

close all
figure
plot(t,ma,t,mb,t,mc,t,hap,t,han)
legend('m_a','m_b','m_c','h_{ap}','h_{an}')
xlabel('time (s)')
ylabel('V_{ln} (p.u.)')
axis([-inf inf -1.2 1.2])

figure
plot(t,mu,t,mv,t,mw,t,hup)
legend('m_a','m_b','m_c','h_{up}')
xlabel('time (s)')
ylabel('V_{ln} (p.u.)')
axis([-inf 0.01 -1.2 1.2])

figure
plot(t,hau)
%legend('m_a','m_b','m_c','h_{ap}','h_{an}')
xlabel('time (s)')
ylabel('V_{ln} (p.u.)')
axis([-inf inf -.2 1.2])

figure
plot(t,vu-vv,t,vv-vw,t,vw-vu)
legend('v_{uv}','v_{vw}','v_{wu}')
xlabel('time (s)')
ylabel('V_{ll} (p.u.)')
axis([-inf inf -1.2 1.2])

figure
plot(t,iu.*hau)
%legend('i_{au}','i_{bu}','i_{cu}')
xlabel('time (s)')
ylabel('I_{au} (A)')
axis([-inf 0.0167 -inf inf])

figure
plot(t,va-vu)
%legend('v_{au}','v_{bu}','v_{cu}')
xlabel('time (s)')
ylabel('V_{throw} (V)')
axis([-inf 0.0167 -inf inf])

figure
plot(t,vb-vu)
%legend('v_{au}','v_{bu}','v_{cu}')
xlabel('time (s)')
ylabel('V_{throw} (V)')
axis([-inf 0.0167 -inf inf])

figure
plot(t,vc-vu)
%legend('v_{au}','v_{bu}','v_{cu}')
xlabel('time (s)')
ylabel('V_{throw} (V)')
axis([-inf 0.0167 -inf inf])