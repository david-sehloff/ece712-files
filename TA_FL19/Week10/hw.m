R = 50;
C = 10*1e-12;
H = tf([R*C 0],[R*C 1])
points = 1e8;
% signal = zeros(1,points);
t_r = 20e-9; %(s)
f_base = 200; %(Hz)
T = 1/f_base; %(s)
V_mag = 350;

fmin = f_base;
fmax = 1e6;
n = 1:ceil(fmax/fmin);
 
points_tr = round(t_r/T)
points_tf = round(t_r/T)
signal(1:points_tr) = linspace(0, V_mag, points_tr);
signal((points_tr+1):round(points/2)) = V_mag;
signal((round(points/2)+1):(round(points/2)+points_tf)) = linspace(V_mag, 0, points_tf);

freqs([R*C 0],[R*C 1]);
figure
plot(s)
c = V_mag * (sin(n*pi/2)./(n * pi / 2)) .* (sin(n*pi*t_r/T)./(n * pi * t_r / T)) .* exp(-1j*pi*n*(0.5+t_r/T));

h = (R*C*n/T) ./ (R*C*n/T + 1);

Vr = h.*c;
vr=ifft(Vr);
% cmag = V_mag * (sin(n*pi/2)./(n * pi / 2)) .* (sin(n*pi*t_r/T)./(n * pi * t_r / T));

% figure
%stem(abs(vr))

plot(abs(vr))
