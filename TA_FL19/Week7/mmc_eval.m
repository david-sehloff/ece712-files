vs = 35;

v_in_rms = 480;
v_in_pk_ln = v_in_rms * sqrt(2) / sqrt(3);
f_in = 60;

v_link_pk = 240;
v_link_pk_ln = v_link_pk/2;
f_link = 1000;

v_out_rms = 208;
v_out_pk_ln = v_out_rms * sqrt(2) / sqrt(3);
f_out = 50;

P_rated = 1e4;

num_periods = 1;

t = linspace(0,num_periods/(min([f_in,f_link,f_out])),1e3);
van = v_in_pk_ln*cos(2*pi*f_in*t);
vbn = v_in_pk_ln*cos(2*pi*f_in*t-2*pi/3);
vcn = v_in_pk_ln*cos(2*pi*f_in*t+2*pi/3);

vxn = v_link_pk_ln*cos(2*pi*f_link*t);
vyn = -v_link_pk_ln*cos(2*pi*f_link*t);

vpn = v_in_pk_ln*cos(2*pi*f_out*t);
vqn = v_in_pk_ln*cos(2*pi*f_out*t-2*pi/3);
vrn = v_in_pk_ln*cos(2*pi*f_out*t+2*pi/3);

ia = P_rated*sqrt(6)/(3*v_in_rms)*cos(2*pi*f_in*t);
ib = P_rated*sqrt(6)/(3*v_in_rms)*cos(2*pi*f_in*t-2*pi/3);
ic = P_rated*sqrt(6)/(3*v_in_rms)*cos(2*pi*f_in*t+2*pi/3);

ix = P_rated*2/v_link_pk*cos(2*pi*f_link*t);
iy = -P_rated*2/v_link_pk*cos(2*pi*f_link*t);

ip = P_rated*sqrt(6)/(3*v_out_rms)*cos(2*pi*f_out*t);
iq = P_rated*sqrt(6)/(3*v_out_rms)*cos(2*pi*f_out*t-2*pi/3);
ir = P_rated*sqrt(6)/(3*v_out_rms)*cos(2*pi*f_out*t+2*pi/3);

n1 = ceil((v_in_pk_ln + v_link_pk_ln)/vs);
n2 = ceil((v_out_pk_ln + v_link_pk_ln)/vs);

varm_an_xn = van - vxn;
iarm_an_xn = ia/2 + ix/3;
marm_an_xn = varm_an_xn/(n1*vs);

% Other modulation functions
marm_an_yn = (van - vyn)/(n1*vs);
marm_bn_xn = (vbn - vxn)/(n1*vs);
marm_bn_yn = (vbn - vyn)/(n1*vs);
marm_cn_xn = (vcn - vxn)/(n1*vs);
marm_cn_yn = (vcn - vyn)/(n1*vs);

marm_xn_pn = (vxn - vpn)/(n2*vs);
marm_yn_pn = (vyn - vpn)/(n2*vs);
marm_xn_qn = (vxn - vqn)/(n2*vs);
marm_yn_qn = (vyn - vqn)/(n2*vs);
marm_xn_rn = (vxn - vrn)/(n2*vs);
marm_yn_rn = (vyn - vrn)/(n2*vs);

% Conduction Losses for a single arm
vb = varm_an_xn;
ib = iarm_an_xn;
mb = marm_an_xn;
id1 = ib.*mb.*(vb>0).*(ib>0) + 0.5*ib.*(1-abs(mb)).*(ib>0);
id4 = id1;
id2 = -ib.*mb.*(vb<0).*(ib<0) + 0.5*ib.*(1-abs(mb)).*(ib<0);
id3 = id2;
it1 = ib.*mb.*(vb>0).*(ib<0) + 0.5*ib.*(1-abs(mb)).*(ib<0);
it4 = it1;
it2 = -ib.*mb.*(vb<0).*(ib>0) + 0.5*ib.*(1-abs(mb)).*(ib>0);
it3 = it2;
close all
figure
subplot(6,1,1)
plot(t, mb)
ylabel('m_{arm}')
subplot(6,1,2)
plot(t, ib)
ylabel('i_{arm}')
subplot(6,1,3)
plot(t, id1)
ylabel('i_{d1}')
subplot(6,1,4)
plot(t, id2)
ylabel('i_{d2}')
subplot(6,1,5)
plot(t, it1)
ylabel('i_{t1}')
subplot(6,1,6)
plot(t, it2)
ylabel('i_{t2}')

vf = 0.77;
r_on = 7e-3;
p_cond_single_d = vf*id1*n1;
p_cond_single_t = id1.^2*r_on*n1;
figure
subplot(2,1,1)
plot(t, p_cond_single_d)
ylabel('diode conduction losses')
subplot(2,1,2)
plot(t, p_cond_single_t)
ylabel('transistor conduction losses')
xlabel('time (s)')

p_cond_tot_d = mean(p_cond_single_d) * 6
p_cond_tot_t = mean(p_cond_single_t) * 6

fs = 1e4;
toff = 5e-9;
ton = 5e-9;
trr = 5e-9;

p_sw_tot = vs*mean(ib)*fs*(toff+ton+trr)